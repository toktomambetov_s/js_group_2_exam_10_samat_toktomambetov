import React, { Component, Fragment } from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import PostForm from "../../components/PostForm/PostForm";
import {createPost} from "../../store/actions/news";

class NewPost extends Component {
    createPost = postData => {
        this.props.onPostCreated(postData).then(() => {
            this.props.history.push('/');
        })
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Add new post</PageHeader>
                <PostForm onSubmit={this.createPost}/>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPostCreated: postData => {
            return dispatch(createPost(postData))
        }
    }
};

export default connect(null, mapDispatchToProps)(NewPost);
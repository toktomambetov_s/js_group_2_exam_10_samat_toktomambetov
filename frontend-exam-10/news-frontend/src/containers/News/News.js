import React, { Component, Fragment } from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import {fetchPosts} from "../../store/actions/news";

class News extends Component {
    componentDidMount() {
        this.props.onFetchPosts();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Posts
                    <Link to="/posts/new">
                        <Button bsStyle="primary" className="pull-right">
                            Add new post
                        </Button>
                    </Link>
                </PageHeader>

                {this.props.posts.map(post => (
                    <Panel key={post.id}>
                        <Panel.Body>
                            <Link to={'/posts/' + post.id}>
                                {post.title}
                            </Link>
                            <u style={{marginLeft: '10px'}}>
                                At {post.date}
                            </u>
                        </Panel.Body>
                    </Panel>
                ))}

            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts.posts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPosts: () => dispatch(fetchPosts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(News);
import axios from '../../axios-api';
import {CREATE_POST_SUCCESS, FETCH_POSTS_SUCCESS} from "./actionTypes";

export const fetchPostsSuccess = posts => {
    return {type: FETCH_POSTS_SUCCESS, posts};
};

export const fetchPosts = () => {
    return dispatch => {
        axios.get('/posts').then(
            response => dispatch(fetchPostsSuccess(response.data))
        );
    }
};

export const createPostSuccess = () => {
    return {type: CREATE_POST_SUCCESS};
};

export const createPost = postData => {
    return dispatch => {
        return axios.post('/posts', postData).then(
            response => dispatch(createPostSuccess())
        );
    };
};
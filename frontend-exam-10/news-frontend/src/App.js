import React, { Component, Fragment } from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Route, Switch} from "react-router-dom";
import News from "./containers/News/News";
import NewPost from "./containers/NewPost/NewPost";


class App extends Component {
    render() {
        return (
            <Fragment>
                <header><Toolbar/></header>
                <main className="container">
                    <Switch>
                        <Route path="/" exact component={News}/>
                        <Route path="/posts/new" exact component={NewPost}/>
                    </Switch>
                </main>
            </Fragment>
        );
    }
}

export default App;

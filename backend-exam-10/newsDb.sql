DROP DATABASE IF EXISTS `exam_10`;
CREATE DATABASE IF NOT EXISTS `exam_10`;
USE `exam_10`;

CREATE TABLE `posts` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255) NOT NULL,
    `content` TEXT,
	`date` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

CREATE TABLE `comments` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `post_id` INT NOT NULL,
    `author` VARCHAR(255),
    `comment` TEXT,
    PRIMARY KEY (`id`),
    INDEX `FK_post_idx` (`post_id`),
    CONSTRAINT `FK_post`
		FOREIGN KEY (`post_id`)
        REFERENCES `posts` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

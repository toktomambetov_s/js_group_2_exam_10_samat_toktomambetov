const express = require('express');

const router = express.Router();

const createRouter = (db) => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM `posts`', function (error, results) {
            if (error) throw error;

            console.log(results);
            res.send(results);
        })

    });

    router.post('/', (req, res) => {
        const post = req.body;

        console.log(req.body);

        db.query(
            'INSERT INTO `posts` (`title`, `content`) ' +
            'VALUES (?, ?)',
            [post.title, post.content],
            (error, results) => {
                if (error) throw error;

                console.log(results);
                res.send(post);
            }
        );
    });

    // router.get('/:id', (req, res) => {
    //     res.send(db.getDataById(req.params.id));
    // });

    return router;
};

module.exports = createRouter;
const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const posts = require('./app/posts');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '0777253269s',
    database: 'exam_10'
});

connection.connect((err) => {
    if (err) throw err;

    app.use('/posts', posts(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
